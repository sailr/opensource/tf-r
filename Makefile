.PHONY: build
build:
	go build -o bin

.PHONY: run-bin
run-bin: build
	./bin/tf-r

.PHONY: run
run:
	go run main.go

.PHONY: docker-build
docker-build:
	docker build -t registry.gitlab.com/sailr/opensource/tf-r .

.PHONY: docker-push
docker-push:
	docker push registry.gitlab.com/sailr/opensource/tf-r

.PHONY: docker-run
docker-run:
	docker run -p 3030:3030 registry.gitlab.com/sailr/opensource/tf-r

.PHONY: docker-shell
docker-shell:
	docker run -it registry.gitlab.com/sailr/opensource/tf-r bash
