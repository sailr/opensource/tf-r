package controller

import (
	"fmt"
	"time"
)

// ControlLoop handles reconciliation on cadence
func ControlLoop() {
	ticker := time.NewTicker(3 * time.Second)

	done := make(chan bool)
	go func() {
		for {
			select {
			case <-done:
				return
			case t := <-ticker.C:
				fmt.Println("Tick at", t)
			}
		}
	}()

	time.Sleep(5 * time.Second)
	ticker.Stop()
	done <- true
	fmt.Println("Ticker stopped")
}
