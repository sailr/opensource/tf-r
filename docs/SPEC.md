# tf-R

Declarative state reconciliation model enforced by a control loop.

## Overview

Configuration drift falls into two specific buckets:

* Expected State Conditions
* Actual State Conditions

The difference between those two states result in a *Reconciliation path* (the method used to bring the two states back together).

The following can occur

1. State is described and applied
2. Error states are described and reconciliation methods are supplied
3. `tf-R` is notified of the state applied, and is given a method to check that state
4. The applied state is hashed and stored.The hash is used to decipher state change (actual v expected)
5. If the hashes do not match, the actual state is read and compared with the expected state.
6. A parser parses the diff and (if preprogrammed) tries to reconcile the issue based on the steps it was given (a database is kept of issues and possible resolutions)
7. The user is notified of the configuration drift via async communication (i.e. email, slack, etc)

The format to describe state is done in yaml

An API is provided to handle parsing.

### Data Model

Desired State:

Expected State:

Reconciled State:
