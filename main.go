package main

import (
	"gitlab.com/sailr/opensource/tf-r/pkg/controller"
	"gitlab.com/sailr/opensource/tf-r/pkg/server"
)

func main() {
	go controller.ControlLoop()
	server.Start()
}
